# Generated by Django 2.2.5 on 2019-10-06 05:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='todo',
            name='tanggal',
        ),
        migrations.AddField(
            model_name='todo',
            name='date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='todo',
            name='time',
            field=models.DateTimeField(null=True),
        ),
    ]
