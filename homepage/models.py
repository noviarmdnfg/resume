from django.db import models

# Create your models here.
class Todo(models.Model):
	date = models.DateField()
	time = models.TimeField()
	where = models.TextField(null=True)
	category = models.TextField(null=True)
	todo = models.TextField()

			