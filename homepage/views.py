from django.shortcuts import render
from .form import Add_TodoList
from .models import Todo
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime
from django.shortcuts import get_object_or_404

response = {
	'nama' : 'Novia Ramadani'
}
# Create your views here.
def abilities(request):
	return render(request, 'Abilities.html')

def contact(request):
	return render(request, 'Contact.html')

def education (request):
	return render(request, 'Education.html')

def experience (request):
	return render(request, 'Experience.html')

def home(request):
	return render(request, 'home.html')

def homepage(request):
	return render(request, 'homepage.html')

def todo(request):
	Todolist = Todo.objects.all().values()
	response['Todolist'] = convert_queryset_into_json(Todolist)
	response['form'] = Add_TodoList

	return render(request, 'jadwal.html', response)

def send(request):
	form = Add_TodoList(request.POST)

	if request.method == 'POST' and form.is_valid():
		print(request.POST)
		date_converted = datetime.strptime(request.POST['date'], '%Y-%m-%d')
		time_converted = datetime.strptime(request.POST['time'], '%H:%M')
		log = Todo(date=date_converted, time=time_converted, where=request.POST['where'], category=request.POST['category'], todo=request.POST['todo'])
		log.save()

	return HttpResponseRedirect('/schedule/')
	
def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def delete_schedule(request, id=None):
	todoObject = get_object_or_404(Todo, pk=id)
	todoObject.delete()
	return HttpResponseRedirect('/schedule/')
