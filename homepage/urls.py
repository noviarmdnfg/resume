from django.urls import re_path
from . import views

app_name = 'homepage'

urlpatterns = [
	re_path(r'^$', views.homepage, name ='homepage'),
	re_path(r'^home/$', views.home, name='home'),
	re_path(r'^abilities/$', views.abilities, name = 'abilities'),
	re_path(r'^contact/$', views.contact, name='contact'),
	re_path(r'^education/$', views.education, name='education'),
	re_path(r'^experience/$', views.experience, name = 'experience'),
	re_path(r'^schedule/$', views.todo, name = 'schedule'),
	re_path(r'^send/$', views.send, name = 'send'),
	re_path(r'^(?P<id>[0-9]+)/delete_schedule/$', views.delete_schedule, name= 'delete_schedule')
]