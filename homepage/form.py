from django import forms


class Add_TodoList(forms.Form):

	attr = {
	'class' : 'form-control'
	}

	date = forms.DateField(label='Tanggal', required=True, widget=forms.DateInput(attrs=attr))
	time = forms.TimeField(label='Waktu', required=True, widget=forms.TimeInput(attrs=attr))
	where = forms.CharField(label='Tempat', required=True, widget=forms.TextInput(attrs=attr))
	category = forms.CharField(label='Kategori', required=True, widget=forms.TextInput(attrs=attr))
	todo = forms.CharField(label='Jadwal', required=True, widget=forms.TextInput(attrs=attr))