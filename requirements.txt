Django==2.2.5
pytz==2019.2
sqlparse==0.3.0
gunicorn
whitenoise==4.1.4
